# ci-template



## Getting started

To use this template, copy the helm chart to your repo and includ the gitlab lab pipeline file

create gitlab ci file and add the include session
```
touch .gitlab-ci.yml
```

```yml
include:
  - project: 'dev-con/ci-template'
    ref: main
    file: '.gitlab-ci.yml'
```

## Healthcheck
Please make sure to add `/healthcheck` route and it should return `200`

```javascript
app.use('/healthcheck', function(req,res){
  res.status(200)
  res.send("App is running")
});
```

## Service Port

Make sure that service is listening on port 3000

```javascript
/**
 * Get port from environment and store in Express.
 */

var port = normalizePort(process.env.PORT || '3000');
app.set('port', port);
```


## Service to service communication 

For internal service communication the service should be reachable

```shell
reponame.branch-name-projectname
```

## Ingress

The ingress of the service will be

reponame.terradevops.com



